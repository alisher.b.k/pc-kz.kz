<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TelegramUsers;
class Patient extends Model
{
    protected $fillable = [
        'telegram_users_id', 'insuredOrNot', 'cardNumber', 'phoneNumber' , 'selectedClinic' , 'selectedDoctor' , 'reasonForTheRequest'
    ];
    public function telegramUsers(){
        return $this->belongsTo(TelegramUsers::class);
    }
}
