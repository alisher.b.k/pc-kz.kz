<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Telegram\DefaultKeyboard;
use App\TelegramRequestLog;
use App\Telegram\DoctorCommand;
use App\TelegramUsers;
use App\Telegram\VacationCommand;
use App\Telegram\StartCommand;
use App\Patient;
use App\SaveData;
use App\Telegram\MainCommand;
use App\Telegram\MenuCommand;
use App\Telegram\SubscriptionCommand;
use App\Messages;
use App\MessagesTelegramUsers;
use Illuminate\Support\Str;
class TelegramHandlerController extends Controller
{
    public function index(){

        $update =  Telegram::commandsHandler(true);
        $message = $update->getMessage();
        $chat_id = $message->getChat()->getId();
        $username = $message->getChat()->getUsername();
        $firstname = $message->getChat()->first_name;
        $lastname = $message->getChat()->last_name;
        $messageText = $message->getText();
        $telegramUsers = TelegramUsers::where('telegramId',$chat_id)->first();
        $typesOfMessages = Messages::all();
        if(!isset($telegramUsers)){
            $user = new TelegramUsers();
            $user->telegramId = $chat_id;
            $user->username = $username;
            $user->firstname = $firstname;
            $user->lastname = $lastname;
            $user->save();
            foreach ($typesOfMessages as $type) {
                $messageAndUsers = new MessagesTelegramUsers();
                $messageAndUsers->messages_id = $type->id;
                $messageAndUsers->telegram_users_id = $user->id;
                $messageAndUsers->save();
            }
        }
        if(isset($update['callback_query'])){
            $callbackQuery = $update->getCallbackQuery();
            $callbackQueryId = $callbackQuery->getId();
            $chatId = $callbackQuery->getMessage()->getChat()->id;
            $messageId = $callbackQuery->getMessage()->message_id;
            $callback = $callbackQuery->getData();
            if(Str::contains($callback, '#poll')){
                GroupController::saveReaction($update);
            }
        }
        $telegramRequestLog = TelegramRequestLog::where('telegramId', $chat_id)->orderBy('created_at','desc')->first();

        if($telegramRequestLog !== null && !in_array($messageText,DefaultKeyboard::getCommands())){
            if($telegramRequestLog->command === 'doc:answerYesOrNo' && $messageText === '✅ Да'){
                Doctorcommand::answerYes($chat_id,$username,$firstname,$lastname,$messageText);
            }elseif($telegramRequestLog->command === 'doc:answerYesOrNo' && $messageText === '❌ Нет'){
                Doctorcommand::answerNo($chat_id,$username,$firstname,$lastname,$messageText);
            }elseif($telegramRequestLog->command === 'doc:numberOfcard' && $messageText !== 'На главную' && $messageText !== 'на главную' && $messageText !== '🔚 На главную'){
                if(strlen($messageText) >= 8){
                    DoctorCommand::numberOfcard($chat_id,$username,$firstname,$lastname,$messageText);
                }else{
                    DoctorCommand::answerYesAgain($chat_id,$username,$firstname,$lastname,$messageText);
                }
            }elseif($telegramRequestLog->command === 'doc:PhoneNumber' && $messageText !== 'На главную' && $messageText !== 'на главную' && $messageText !== '🔚 На главную'){
                if(strlen($messageText) === 11 || strlen($messageText) === 12){
                    Doctorcommand::chooseClinic($chat_id,$username,$firstname,$lastname,$messageText);
                }else{
                    Doctorcommand::phoneNumberOfInsured($chat_id,$username,$firstname,$lastname,$messageText);
                }
            }elseif($telegramRequestLog->command === 'doc:PhoneNumberOfUninsured' && $messageText !== 'На главную' && $messageText !== 'на главную' && $messageText !== '🔚 На главную'){
                if(strlen($messageText) === 11 || strlen($messageText) === 12 ){
                    Doctorcommand::chooseClinic($chat_id,$username,$firstname,$lastname,$messageText);
                }else{
                    Doctorcommand::phoneNumberOfUninsured($chat_id,$username,$firstname,$lastname,$messageText);
                }
            }elseif($telegramRequestLog->command === 'doc:chooseClinic' && $messageText !== 'На главную' && $messageText !== 'на главную' && $messageText !== '🔚 На главную'){
                DoctorCommand::chooseDoctor($chat_id,$username,$firstname,$lastname,$messageText);
            }elseif(isset($callback)){
                    if($telegramRequestLog->command === 'doc:chooseDoctor'){
                        DoctorCommand::reason($chat_id,$username,$firstname,$lastname,$messageText,$callback);
                    }elseif($telegramRequestLog->command === 'sub:YesOrNo'){
                        SubscriptionCommand::yesOrNo($messageId,$callback,$chatId);
                    }
            }elseif($telegramRequestLog->command === 'doc:writeReason' && $messageText !== 'На главную' && $messageText !== 'на главную' && $messageText !== '🔚 На главную'){
                DoctorCommand::finalMessage($chat_id,$username,$firstname,$lastname,$messageText);
            }elseif($telegramRequestLog->command === 'doc:finalMessage' && $messageText === 'Отправить'){
                DoctorCommand::sendToCoordinators($chat_id);
            }elseif($telegramRequestLog->command === 'doc:finalMessage' && $messageText === 'Отменить'){
                DoctorCommand::deleteAllData($chat_id);
            }elseif($messageText === 'На главную' || $messageText === 'на главную' || $messageText === '🔚 На главную'){
                MainCommand::executeCommand($chat_id,$username,$firstname,$lastname);
            }elseif($messageText === '👩‍⚕️ Запись на прием к врачу' || $messageText === 'Запись на прием к врачу' || $messageText === 'doctor' || $messageText === 'Doctor'){
                DoctorCommand::executeCommand($chat_id,$username,$firstname,$lastname);
            }elseif($messageText === '🏝 Мой отпуск' || $messageText === 'Мой отпуск' || $messageText === 'vacation' || $messageText === 'Vacation'){
                VacationCommand::executeCommand($chat_id,$username,$firstname,$lastname);
            }elseif($messageText === '📝 Подписка' || $messageText === 'Подписка' || $messageText === 'Подписка'){
                SubscriptionCommand::executeCommand($chat_id);
            }
        }elseif($messageText === '👩‍⚕️ Запись на прием к врачу' || $messageText === 'Запись на прием к врачу' || $messageText === 'doctor' || $messageText === 'Doctor'){
            DoctorCommand::executeCommand($chat_id,$username,$firstname,$lastname);
        }elseif($messageText === '🏝 Мой отпуск' || $messageText === 'Мой отпуск' || $messageText === 'vacation' || $messageText === 'Vacation'){
            VacationCommand::executeCommand($chat_id,$username,$firstname,$lastname);
        }elseif($messageText === 'На главную' || $messageText === 'на главную' || $messageText === '🔚 На главную'){
            MainCommand::executeCommand($chat_id,$username,$firstname,$lastname);
        }elseif($messageText === '📝 Подписка' || $messageText === 'Подписка' || $messageText === 'Подписка'){
            SubscriptionCommand::executeCommand($chat_id);
        }
    }
}