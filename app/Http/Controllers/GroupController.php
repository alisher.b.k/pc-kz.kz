<?php

namespace App\Http\Controllers;

use GuzzleHttp\Middleware;
use Illuminate\Http\Request;
use App\TypeOfMessages;
use App\TelegramUsers;
use PDO;
use App\Messages;
use Telegram\Bot\Laravel\Facades\Telegram;
use Illuminate\Support\Facades\DB;
use Telegram\Bot\FileUpload\InputFile;
use App\News;
use Telegram\Bot\Keyboard\Keyboard;
use App\Telegram\DefaultKeyboard;
use App\NewsReactions;
use App\TelegramRequestLog;
class GroupController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        return view('group',['messages' => Messages::withCount('telegramUsers')->get(),'users'=>TelegramUsers::all()]);
    }
    public function sendNotifications(Request $request){
        $fileName = "";
        if($request->messageType === "Message with a reaction"){
            $content = preg_replace("/\r|\n/", "", $request->message);
            $news = new News();
            $news->content = $content;
            $news->link = $request->link;
            $news->save();
            
            if($request->hasFile('photo')){
                $file = $request->file('photo');
                $originFileName = substr($file->getClientOriginalName(), 0, strlen($file->getClientOriginalName()) - strlen($file->getClientOriginalExtension()) - 1);
                $date = date('m/d/Y h:i:s a', time());
                $fileName = md5($date . $originFileName) . '.' . $file->getClientOriginalExtension();
                $file->move(public_path() . '/pictures', $fileName);
                $news->imagePath = '/pictures/'.$fileName;
                $news->save();
            }
            $reply_markup = Keyboard::make([
                'resize_keyboard' => true,
                'one_time_keyboard' => true,
                'inline_keyboard' => [
                    [
                        ['text' =>  '👍', 'callback_data' => '👍#poll'.$news->id],
                        ['text' =>  '🤔', 'callback_data' => '🤔#poll'.$news->id],
                        ['text' =>  '👎', 'callback_data' => '👎#poll'.$news->id],
                    ]]]);
        }
        if($request->check === 'yes'){
            $users = TelegramUsers::all();
            foreach ($users as $user) {
                
                if ($fileName && $request->messageType === "Message with a reaction") {
                    Telegram::sendPhoto([
                        'chat_id' => $user->telegramId,
                        'photo' => new InputFile(base_path() . '/public/pictures/' . $fileName, $fileName),
                    ]);
                }
                Telegram::sendMessage([
                    'chat_id'=>$user->telegramId,
                    'text' => $request->message,
                ]);
            }
        }else{
            $users = TelegramUsers::all();
            foreach($users as $user){
                $types = $user->messages;
                $result = 0;
                if(count($types) !== 0){
                    foreach($types as $type){
                        if($type->name !== $request->messageType){
                            $result += 0;
                        }else{
                            $result += 1;
                        }
                        }
                        if($result !== 1){
                            if($request->messageType === "Message with a reaction"){
                                if ($fileName) {
                                    Telegram::sendPhoto([
                                        'chat_id' => $user->telegramId,
                                        'photo' => new InputFile(base_path() . '/public/pictures/' . $fileName, $fileName),
                                    ]);
                                }
                                Telegram::sendMessage([
                                    'chat_id'=>$user->telegramId,
                                    'text' => $request->message,
                                    'reply_markup' => $reply_markup
                                ]);
                            }else{
                                Telegram::sendMessage([
                                    'chat_id'=>$user->telegramId,
                                    'text' => $request->message,
                                ]);
                            }
                        }
                }else{
                        if($fileName) {
                        Telegram::sendPhoto([
                            'chat_id' => $user->telegramId,
                            'photo' => new InputFile(base_path() . '/public/pictures/' . $fileName, $fileName),
                        ]);
                        }
                        if($request->messageType === "Message with a reaction"){
                            Telegram::sendMessage([
                                'chat_id'=>$user->telegramId,
                                'text' => $request->message,
                                'reply_markup' => $reply_markup
                            ]);
                        }else{
                            Telegram::sendMessage([
                                'chat_id'=>$user->telegramId,
                                'text' => $request->message,
                                ]);   
                        }
                }        
            }
        }

        return redirect()->route('message.index');
    }
    public static function saveReaction($update) {
        TelegramRequestLog::where('telegramId',$update["callback_query"]["from"]["id"])->delete();
        $reply_markup = Keyboard::make(['keyboard' => DefaultKeyboard::getMenuKeyboard(),
        'resize_keyboard' => true,
        'one_time_keyboard' => true
        ]);
        $content = preg_replace("/\r|\n/", "", $update["callback_query"]["message"]["text"]);
        $pieces = explode("#poll", $update["callback_query"]["data"]);
        $newsList = News::where('id', $pieces[1])->get();
        foreach($newsList as $news) {
                $existingReaction = NewsReactions::where('telegramId', $update["callback_query"]["from"]["id"])
                ->where('news_id', $news->id)
                ->where('reaction', $pieces[0])
                ->first();
                if (!$existingReaction) {
                    $news_reactions = new NewsReactions();
                    $news_reactions->reaction = $pieces[0];
                    $news_reactions->telegramId = $update["callback_query"]["from"]["id"];
                    $news_reactions->news_id = $news->id;
                    $news_reactions->save();
                    
                    Telegram::sendMessage([
                        'chat_id' => $update["callback_query"]["from"]["id"],
                        'text' => 'Спасибо за вашу оценку',
                        'reply_markup' => $reply_markup
                
                    ]);
                } 
            }
        
    }

    public function sendTypeOfMessage(Request $request){ 
        $message = Messages::where('name',$request->type)->first();
        $users = TelegramUsers::all();
        $types = $message->telegramUsers;
        $result = count($users) - count($types);
        
        return response()->json(['success'=> $result]);    
    }
}
