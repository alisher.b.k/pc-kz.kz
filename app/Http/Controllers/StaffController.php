<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDO;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\BotanStaff;
use Telegram\Bot\Laravel\Facades\Telegram;

class StaffController extends Controller
{
    public $companyList = [
        'SOS Medical Assistance ТОО'
    ];
    public function updateStaff(Request $request){
        DB::beginTransaction();
        $this->deleteOldData();
        $res = true;
        $data = json_decode($request->mData);
        foreach ($data as $empl){
            $user = new BotanStaff();
            $user->FIO = "$empl->LASTNAME $empl->FIRSTNAME $empl->PARENTNAME";
            $user->position = $empl->DUTY;
            $user->photo = null;
            $user->workPhone = null;
            $user->intPhone = $empl->INNPHONE;
            $user->mobPhone = $empl->MOBILEPHONE;
            $user->email = $empl->EMAIL;
            $user->bday = $empl->BIRTHDAY;
            $user->workEx = $empl->WORKEXP;
            $user->ISN = $empl->ISN;
            $user->NAMELAT = $empl->NAMELAT;
            $user->WORKPLACE = $empl->WORKPLACE;
            $user->MAINDEPT = $empl->MAINDEPT;
            $user->SUBDEPT = $empl->SUBDEPT;
            $user->VACATIONBEG = $empl->VACATIONBEG;
            $user->VACATIONEND = $empl->VACATIONEND;
            $user->TRIPBEG = $empl->TRIPBEG;
            $user->TRIPEND = $empl->TRIPEND;
            $user->LEAVEDATE = $empl->LEAVEDATE;
            try{
                if(!$user->save()){
                    $res = false;
                }
            }catch (\Exception $exception){
                Log::debug('NEW USER CESEC');
                Log::debug($exception->getMessage());
                DB::rollback();
            }
        }
        if($res){
            DB::commit();
        }else{
            DB::rollback();
        }
        return [
            'success' => $res
        ];
    }

    public function deleteOldData(){
        $oldStaff = BotanStaff::whereIn('WORKPLACE', $this->companyList)->get();
        foreach ($oldStaff as $old){
            $old->delete();
        }
    }

    static public function getVacation($iin){
        $client = self::getClient($iin);
        $result = $client->iin_string([
            'Input_IIN' => $iin
        ]);
        return [
            'success' => true,
            'result' => $result->return
        ];
    }
    protected static function getClient($iin){
        $client = new \SoapClient('http://82.200.137.50:90/zup2/ws/ws_botan.1cws?wsdl', [
            'cache_wsdl' => WSDL_CACHE_NONE,
            'trace'      => 1,
            'login'      => 'Admin',
            'password'   => 'ghfdf',
        ]);
        return $client;
    }
}
