<?php
namespace App\Telegram;

use App\TelegramUsers;
use PhpParser\Node\Stmt\TryCatch;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Illuminate\Support\Facades\Log;
use App\TelegramRequestLog;
use App\Messages;

class SubscriptionCommand extends Command
{
    protected $name = 'subscription';

    protected $description = 'Subscription command, Get a list of commands';
    
    public function handle(){
        $update = Telegram::getWebhookUpdates();
        $message = $update->getMessage();
        $chat_id = $message->getChat()->getId();
        $username = $message->getChat()->getUsername();
        $firstname = $message->getChat()->first_name;
        $lastname = $message->getChat()->last_name;
        $messageText = $message->getText();
        $this->executeCommand($chat_id);
    }
    public static function executeCommand($chat_id){
        try {
        TelegramRequestLog::where('telegramId', $chat_id)->delete();
        $log = new TelegramRequestLog();
        $log->telegramId = $chat_id;
        $log->command = 'sub:YesOrNo';
        $log->save();
           
        $text = "\nДобро пожаловать в раздел подписки!\n";
        $data = Messages::all();
        $keyboard = DefaultKeyboard::subscription($data,$chat_id);
        Telegram::sendMessage([
            'chat_id'=>$chat_id,
            'text' => $text,
            'reply_markup' => $keyboard,
        ]);
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
        }
    }
    public static function yesOrNo($messageId,$callback,$chatId){
        TelegramRequestLog::where('telegramId', $chatId)->delete();
        
        $log = new TelegramRequestLog();
        $log->telegramId = $chatId;
        $log->command = 'sub:YesOrNo';
        $log->save();

        $messages = Messages::all();
        $user = TelegramUsers::where('telegramId',$chatId)->first();
        $types = $user->messages;

        $result = 0;
        if(count($types) === 0){
            $result = 0;
        }else{
                foreach($types as $type){
                    if("❌".$type->name === $callback){
                        $result += 1;
                    }else{
                        $result +=0;
                    }
        }}
        if($result === 1){
            foreach($messages as $message){
                if("❌".$message->name === $callback){
                        $user->messages()->detach($message->id);
                }
            } 
        }else{
                foreach($messages as $message){
                    if("✅".$message->name === $callback){
                            $user->messages()->syncWithoutDetaching($message->id);
                    }
                }
        }
        $keyboard = DefaultKeyboard::subscription($messages,$chatId);
        $text = "\nДобро пожаловать в раздел подписки!\n";
        Telegram::editMessageText([
            'text' => $text,
            'chat_id' => $chatId,
            'message_id' => $messageId,
            'reply_markup' => $keyboard
        ]);
    }
 }