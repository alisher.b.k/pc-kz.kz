<?php
namespace App\Telegram;
use Telegram\Bot\Commands\Command;
use App\Telegram\DefaultKeyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use App\TelegramRequestLog;
use App\Clinics;
use Illuminate\Support\Facades\Log;
use App\TelegramUsers;
class DoctorCommand extends Command
{
    
    protected $name = 'doctor';

    protected $description = 'Doctor command, Get a list of commands';
    
    public function handle(){
        $update = Telegram::getWebhookUpdates();
        $message = $update->getMessage();
        $chat_id = $message->getChat()->getId();
        $username = $message->getChat()->getUsername();
        $firstname = $message->getChat()->first_name;
        $lastname = $message->getChat()->last_name;
        $messageText = $message->getText();
        $this->executeCommand($chat_id,$username,$firstname,$lastname);
    }

    public static function executeCommand($chat_id,$username,$firstname,$lastname){
        try {
        TelegramRequestLog::where('telegramId', $chat_id)->delete();
        $log = new TelegramRequestLog();
        $log->telegramId = $chat_id;
        $log->command = 'doc:answerYesOrNo';
        $log->save();
        
        $keyboard = DefaultKeyboard::getAnswer();
        $text = 'Вы застрахованы ?';
        $reply_markup = Keyboard::make([
            'keyboard' => $keyboard, 
            'resize_keyboard' => true, 
            'one_time_keyboard' => true
        ]);
        Telegram::sendMessage([
            'chat_id'=>$chat_id,
            'text' => $text,
            'reply_markup' => $reply_markup,
        ]);
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
        }
        
        
    }

    public static function answerYes($chat_id,$username,$firstname,$lastname,$messageText){
        try {
            TelegramRequestLog::where('telegramId', $chat_id)->delete();

            $logArr = [
                "command" => 'doc:numberOfcard',
                "telegramId" => $chat_id,
                "data" => json_encode(["insuredOrNot" =>'Да']),
              ];
        
            $text = 'Введите номер карточки страхования:';
            $keyboard = DefaultKeyboard::MainKeyboard();
            $reply_markup = Keyboard::make([
                'keyboard' => $keyboard, 
                'resize_keyboard' => true, 
                'one_time_keyboard' => true
            ]);
            Telegram::sendMessage([
            'chat_id'=>$chat_id,
            'text' => $text,
            'reply_markup' => $reply_markup
            ]);
            TelegramRequestLog::create($logArr);
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
        }
    }
    public static function answerYesAgain($chat_id,$username,$firstname,$lastname,$messageText){
        try {
            TelegramRequestLog::where('telegramId', $chat_id)->delete();

            $logArr = [
                "command" => 'doc:numberOfcard',
                "telegramId" => $chat_id,
                "data" => json_encode(["insuredOrNot" =>'Да']),
              ];
        
            $text = 'Введите корректный номер страховой карточки:';
            $keyboard = DefaultKeyboard::MainKeyboard();
            $reply_markup = Keyboard::make([
                'keyboard' => $keyboard, 
                'resize_keyboard' => true, 
                'one_time_keyboard' => true
            ]);
            Telegram::sendMessage([
            'chat_id'=>$chat_id,
            'text' => $text,
            'reply_markup' => $reply_markup
            ]);
            TelegramRequestLog::create($logArr);
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
        }
    }

    public static function numberOfcard($chat_id,$username,$firstname,$lastname,$messageText){
        try {
            $telegramRequestLog = TelegramRequestLog::where('telegramId', $chat_id)->first();
            TelegramRequestLog::where('telegramId', $chat_id)->delete();
            
            $logArr = [
                "command" => 'doc:PhoneNumber',
                "telegramId" => $chat_id,
                "data" => json_decode($telegramRequestLog->data, true),
              ];

            $logArr['data'] = json_encode(array_merge($logArr['data'],["cardNumber" =>$messageText]));

            $text = 'Укажите свой контактный номер телефона в виде +7XXXXXXXXXX:';

            $keyboard = DefaultKeyboard::MainKeyboard();
            $reply_markup = Keyboard::make([
                'keyboard' => $keyboard, 
                'resize_keyboard' => true, 
                'one_time_keyboard' => true
            ]);
            Telegram::sendMessage([
            'chat_id'=>$chat_id,
            'text' => $text,
            'reply_markup' => $reply_markup
            ]);
            TelegramRequestLog::create($logArr);
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
        } 
    }
    public static function phoneNumberOfInsured($chat_id,$username,$firstname,$lastname,$messageText){
        try {
            $telegramRequestLog = TelegramRequestLog::where('telegramId', $chat_id)->first();
            TelegramRequestLog::where('telegramId', $chat_id)->delete();
            
            $logArr = [
                "command" => 'doc:PhoneNumber',
                "telegramId" => $chat_id,
                "data" => json_decode($telegramRequestLog->data, true),
              ];

            $logArr['data'] = json_encode(array_merge($logArr['data'],['phoneNumber' => $messageText]));

            $text = 'Введите корректный номер телефона:';

            $keyboard = DefaultKeyboard::MainKeyboard();
            $reply_markup = Keyboard::make([
                'keyboard' => $keyboard, 
                'resize_keyboard' => true, 
                'one_time_keyboard' => true
            ]);
            Telegram::sendMessage([
            'chat_id'=>$chat_id,
            'text' => $text,
            'reply_markup' => $reply_markup
            ]);
            TelegramRequestLog::create($logArr);
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
        } 

    }
    public static function answerNo($chat_id,$username,$firstname,$lastname){
        try {
            TelegramRequestLog::where('telegramId', $chat_id)->delete();
            
            $logArr = [
                "command" => 'doc:PhoneNumberOfUninsured',
                "telegramId" => $chat_id,
                "data" => json_encode(["insuredOrNot" =>'Нет']),
              ];

            $text = 'Укажите свой контактный номер телефона в виде +7XXXXXXXXXX:';

            $keyboard = DefaultKeyboard::MainKeyboard();
            $reply_markup = Keyboard::make([
                'keyboard' => $keyboard, 
                'resize_keyboard' => true, 
                'one_time_keyboard' => true
            ]);
            Telegram::sendMessage([
            'chat_id'=>$chat_id,
            'text' => $text,
            'reply_markup' => $reply_markup
            ]);
            TelegramRequestLog::create($logArr);
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
        } 
    }

    public static function phoneNumberOfUninsured($chat_id,$username,$firstname,$lastname,$messageText)
    {
        try {
            TelegramRequestLog::where('telegramId', $chat_id)->delete();
            
            $logArr = [
                "command" => 'doc:PhoneNumberOfUninsured',
                "telegramId" => $chat_id,
                "data" => json_encode(["insuredOrNot" =>'Нет']),
              ];

            $text = 'Введите корректный номер телефона:';

            $keyboard = DefaultKeyboard::MainKeyboard();
            $reply_markup = Keyboard::make([
                'keyboard' => $keyboard, 
                'resize_keyboard' => true, 
                'one_time_keyboard' => true
            ]);
            Telegram::sendMessage([
            'chat_id'=>$chat_id,
            'text' => $text,
            'reply_markup' => $reply_markup
            ]);
            TelegramRequestLog::create($logArr);
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
        }
    }
    
    public static function chooseClinic($chat_id,$username,$firstname,$lastname,$messageText){
        try {
          $telegramRequestLog = TelegramRequestLog::where('telegramId', $chat_id)->first();
          TelegramRequestLog::where('telegramId', $chat_id)->delete();

          $logArr = [
            "command" => 'doc:chooseClinic',
            "telegramId" => $chat_id,
            "data" => json_decode($telegramRequestLog->data, true),
          ];
            $logArr['data'] = json_encode(array_merge($logArr['data'],['phoneNumber' => $messageText]));
            $keyboard = DefaultKeyboard::getClinic();
            $text = 'Выберите клинику:';
            
            $reply_markup = Keyboard::make([
                'keyboard' => $keyboard, 
                'resize_keyboard' => true, 
                'one_time_keyboard' => true
            ]);
            Telegram::sendMessage([
                'chat_id'=>$chat_id,
                'text' => $text,
                'reply_markup' => $reply_markup,
            ]);
            TelegramRequestLog::create($logArr);
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
        }
        
    }
    
    public static function chooseDoctor($chat_id,$username,$firstname,$lastname,$messageText){
        
        try {
        
        if($messageText === '🏥 Клиника на Толе би,111'){
            $telegramRequestLog = TelegramRequestLog::where('telegramId', $chat_id)->first();
            TelegramRequestLog::where('telegramId', $chat_id)->delete();
  
            $logArr = [
              "command" => 'doc:chooseDoctor',
              "telegramId" => $chat_id,
              "data" => json_decode($telegramRequestLog->data, true),
            ];

            $logArr['data'] = json_encode(array_merge($logArr['data'],['selectedClinic'=> 'Клиника на Толе би,111']));

            $clinic = Clinics::where('name','Клиника на Толе би,111')->first();
            $text = "\nСпециалисты:\n";
            $keyboard = DefaultKeyboard::getDoctors($clinic);
            Telegram::sendMessage([
                'chat_id' => $chat_id,
                'text' => $text,
                'reply_markup' => $keyboard
            ]);
            TelegramRequestLog::create($logArr);
        }elseif($messageText === '🏥 Клиника на Достык проспект,91/2'){
            $telegramRequestLog = TelegramRequestLog::where('telegramId', $chat_id)->first();
            TelegramRequestLog::where('telegramId', $chat_id)->delete();
  
            $logArr = [
              "command" => 'doc:chooseDoctor',
              "telegramId" => $chat_id,
              "data" => json_decode($telegramRequestLog->data, true),
            ];

            $logArr['data'] = json_encode(array_merge($logArr['data'],['selectedClinic'=> 'Клиника на Достык проспект,91/2']));
            $clinic = Clinics::where('name','Клиника на Достык проспект,91/2')->first();
            $text = "\nСпециалисты:\n";
            $keyboard = DefaultKeyboard::getDoctors($clinic);
            Telegram::sendMessage([
                'chat_id' => $chat_id, 
                'text' => $text,
                'reply_markup' => $keyboard
            ]);
            TelegramRequestLog::create($logArr);
        }    
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
        }
       
    }
    public static function reason($chat_id,$username,$firstname,$lastname,$messageText,$callback){
            try {
                $telegramRequestLog = TelegramRequestLog::where('telegramId', $chat_id)->first();
                TelegramRequestLog::where('telegramId', $chat_id)->delete();
      
                $logArr = [
                  "command" => 'doc:writeReason',
                  "telegramId" => $chat_id,
                  "data" => json_decode($telegramRequestLog->data, true),
                ];
                $logArr['data'] = json_encode(array_merge($logArr['data'],['selectedDoctor'=>$callback]));
    
                $text = 'Укажите причину вашего обращения к врачу:';
                $keyboard = DefaultKeyboard::MainKeyboard();
                $reply_markup = Keyboard::make([
                    'keyboard' => $keyboard, 
                    'resize_keyboard' => true, 
                    'one_time_keyboard' => true
                ]);
                Telegram::sendMessage([
                    'chat_id' => $chat_id, 
                    'text' => $text,
                    'reply_markup' => $reply_markup
                ]);
                TelegramRequestLog::create($logArr);
            } catch (\Throwable $th) {
                Log::debug($th->getMessage());
            }
           
    }
    public static function finalMessage($chat_id,$username,$firstname,$lastname,$messageText){
            
            try {
                $telegramRequestLog = TelegramRequestLog::where('telegramId', $chat_id)->first();
                TelegramRequestLog::where('telegramId', $chat_id)->delete();
      
                $logArr = [
                  "command" => 'doc:finalMessage',
                  "telegramId" => $chat_id,
                  "data" => json_decode($telegramRequestLog->data, true),
                ];
                $logArr['data'] = json_encode(array_merge($logArr['data'],['reasonForTheRequest'=>$messageText]));
                $text = 'Если данные верны нажмите кнопку: "Отправить"';
            
                $keyboard = [['Отправить', 'Отменить']];
                $reply_markup = Keyboard::make([
                    'keyboard' => $keyboard, 
                    'resize_keyboard' => true, 
                    'one_time_keyboard' => true
                ]);
                Telegram::sendMessage([
                'chat_id' => $chat_id, 
                'text' => $text,
                'reply_markup' => $reply_markup
            ]);
                TelegramRequestLog::create($logArr);
            } catch (\Throwable $th) {
                Log::debug($th->getMessage());
            }
        }
    public static function sendToCoordinators($chat_id){
        $telegramRequestLog = TelegramRequestLog::where('telegramId', $chat_id)->first();
        TelegramRequestLog::where('telegramId', $chat_id)->delete();
        $data = json_decode($telegramRequestLog->data,true);
        $telegramUser = TelegramUsers::where('telegramId', $chat_id)->first();
        $text  =     "\n Имя: ".$telegramUser['firstname'].
                    "\nФамилия: ".$telegramUser['lastname'].
                    "\nКонтактный номер : ".$data['phoneNumber'].
                    "\nКлиника : ".$data['selectedClinic'].
                    "\nВрач : ".$data['selectedDoctor'].
                    "\nПричина обращения : ".$data['reasonForTheRequest'].
                    "\nЗастрахован : ".$data['insuredOrNot'];

        if(isset($data['cardNumber'])){
            $text.= "\nНомер страховой карточки : ".$data['cardNumber'];
        }
        Telegram::sendMessage([
            'chat_id' => 1618342840,
            'text' => $text,
        ]);
        $keyboard  = DefaultKeyboard::getMenuKeyboard();
        $reply_markup = Keyboard::make([
            'keyboard' => $keyboard, 
            'resize_keyboard' => true, 
            'one_time_keyboard' => true
        ]);
        $text_final = "Спасибо за обращение! Пожалуйста, дождитесь звонка от сотрудника call-центра с информацией о вашей записи";
        Telegram::sendMessage([
            'chat_id' => $chat_id,
            'text' => $text_final, 
            'reply_markup' => $reply_markup
        ]);
    } 
     
    public static function deleteAllData($chat_id){
        $keyboard  = DefaultKeyboard::getMenuKeyboard();
        TelegramRequestLog::where('telegramId', $chat_id)->delete();
        $text = 'Bye!';
        $reply_markup = Keyboard::make([
            'keyboard' => $keyboard, 
            'resize_keyboard' => true, 
            'one_time_keyboard' => true
        ]);
        Telegram::sendMessage([
        'chat_id' => $chat_id,
        'text' => $text,
        'reply_markup' => $reply_markup
    ]);

    } 
}
