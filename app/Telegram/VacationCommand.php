<?php
namespace App\Telegram;

use App\Http\Controllers\StaffController;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Commands\Command;
class VacationCommand extends Command
{
    protected $name = 'vacation';

    protected $description = 'Vacation command, Get a list of commands';
    public function handle(){
        $update = Telegram::getWebhookUpdates();
        $message = $update->getMessage();
        $chat_id = $message->getChat()->getId();
        $username = $message->getChat()->getUsername();
        $firstname = $message->getChat()->first_name;
        $lastname = $message->getChat()->last_name;
        $messageText = $message->getText();
        $this->executeCommand($chat_id,$username,$firstname,$lastname);
    }
    public static function executeCommand($chat_id,$username,$firstname,$lastname){
        $vacationData = StaffController::getVacation('960207300361');
        $reply_markup = Keyboard::make([
            'keyboard' => DefaultKeyboard::getMenuKeyboard(),
            'resize_keyboard' => true,
            'one_time_keyboard' => true
        ]);
        Telegram::sendMessage([
            'chat_id' => $chat_id,
            'text' => 'Осталось дней отпуска: '.$vacationData['result'],
            'reply_markup' => $reply_markup
        ]);
    }
}