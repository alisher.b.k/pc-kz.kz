<?php
namespace App\Telegram;
use App\Clinics;
use Telegram\Bot\Keyboard\Keyboard;
use App\Messages;
use App\MessagesTelegramUsers;
use App\TelegramUsers;
use PDO;

class DefaultKeyboard {

    public static function getMenuKeyboard(){
        $keyboard = [
            ['👨‍💼 Найду коллегу', '🏝 Мой отпуск'],
            ['🏦 О нас', '💡 Заявить о проблеме'],
            ['✉️ Улучшить Ботан','👩‍⚕️ Запись на прием к врачу'],
            ['📝 Подписка']
         ];
         return $keyboard;
    }

    public static function MainKeyboard(){
        $keyboard = [
            ['🔚 На главную']
        ];
        return $keyboard;
    }
    
    public static function getAnswer(){
        $keyboard = [
            ['✅ Да','❌ Нет'],
            ['🔚 На главную']
        ];
        return $keyboard;
    }
    
    public static function getCommands()
    {
        $commands = [
            '/start', '/vacation', '/facebook',
            '/problem', '/culture','/feedback',
            '/doctor'
        ];
        return $commands;
    }
    public static function getClinic()
    {
       $clinics = Clinics::all();
       $keyboard = [];
       foreach ($clinics as $clinic) {
            array_push($keyboard,[
                '🏥 '.$clinic->name
            ]);
       }
       array_push($keyboard,['🔚 На главную']);
       return $keyboard;
    }

    public static function getDoctors($clinic)
    {   
        $keyboard = Keyboard::make()->inline();
        $key = [];
        foreach($clinic->doctors as  $clc){
        $key[] = Keyboard::inlineButton([
            'text'          => $clc->specialty,
            'callback_data' => $clc->specialty,
        ]);
        if(count($key) === 2){
            $keyboard->row($key[0],$key[1]);
            $key = [];
        }
        }
        if(count($key) > 0){
            $keyboard->row($key[0]);
        }
        return $keyboard;  
    }
    static function subscription($data,$chat_id){
        $user = TelegramUsers::where('telegramId',$chat_id)->first();
        $result = [];
        foreach($data as $d){
            $types = $user->messages;
            $curr = 0;

            if(count($types) === 0){
                array_push($result,"✅".$d->name);
            }else{
                foreach($types as $type){
                    if($d->id === $type->pivot->messages_id){
                        $curr += 1;
                    }else{
                        $curr += 0;
                    }
                }

                if($curr == 1){
                    array_push($result,"❌".$d->name);
                }else{
                    array_push($result,"✅".$d->name);
                }
            }
        }
        $keyboard = Keyboard::make()->inline();
        $key = [];
        foreach($result as $message){
                $key[] = Keyboard::inlineButton([
                    'text'          => $message,
                    'callback_data' => $message,
                ]);
            if(count($key) === 2){
                $keyboard->row($key[0],$key[1]);
                $key = [];
            }
        }                           
        if(count($key) > 0){
            $keyboard->row($key[0]);
        }
        return $keyboard;  
    }
}       