<?php

namespace App\Telegram;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use App\TelegramRequestLog;

class MainCommand
{
    public static function executeCommand($chat_id,$username,$firstname,$lastname){
        TelegramRequestLog::where('telegramId', $chat_id)->delete();
        $text =  "\nВот, что я умею:\n".
                  "👨‍💼 Найду коллегу /facebook".
                "\n"."🏝 Мой отпуск /vacation".
                "\n"."🏦 О нас /holding".
                "\n"."💡 Заявить о проблеме /problem".
                "\n"."✉️ Улучшить Ботан /feedback".
                "\n"."👩‍⚕️ Запись на прием к врачу  /doctor".
                "\n"."📝 Подписка /subscription";
            $keyboard  = DefaultKeyboard::getMenuKeyboard();
            $reply_markup = Keyboard::make([
                'keyboard' => $keyboard, 
                'resize_keyboard' => true, 
                'one_time_keyboard' => true
            ]);
            Telegram::sendMessage([
                'chat_id'=>$chat_id,
                'text' => $text,
                'reply_markup' => $reply_markup,
            ]);

      }
}