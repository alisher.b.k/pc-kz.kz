<?php

namespace App\Telegram;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Actions;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use App\Telegram\DefaulKeyboard;
/**
 * Class HelpCommand.
 */
class StartCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'start';

    protected $description = 'Start command, Get a list of commands';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {  
       $update = Telegram::getWebhookUpdates();
       $message = $update->getMessage();
       $chat_id = $message->getChat()->getId();
       $username = $message->getChat()->getUsername();
       $firstname = $message->getChat()->first_name;
       $lastname = $message->getChat()->last_name;
       $messageText = $message->getText();

       $this->executeCommand($chat_id,$username,$firstname,$lastname);
    }

       public function  executeCommand($chat_id,$username,$firstname,$lastname){
             
       $this->replyWithChatAction(['action'=>Actions::TYPING]);
       $text =  "\nВот, что я умею:\n".
                  "👨‍💼 Найду коллегу /facebook".
                "\n"."🏝 Мой отпуск /vacation".
                "\n"."🏦 О нас /holding".
                "\n"."💡 Заявить о проблеме /problem".
                "\n"."✉️ Улучшить Ботан /feedback".
                "\n"."👩‍⚕️ Запись на прием к врачу  /doctor".
                "\n"."📝 Подписка /subscription";
      $keyboard  = DefaultKeyboard::getMenuKeyboard();
      $reply_markup = Keyboard::make([
            'keyboard' => $keyboard, 
            'resize_keyboard' => true, 
            'one_time_keyboard' => true
      ]);
      $this->replyWithMessage(['text' => $text, 'reply_markup' => $reply_markup]);
}
}