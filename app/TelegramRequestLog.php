<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TelegramRequestLog extends Model
{
    protected $fillable = [
        'telegramId', 'command', 'data',
      ];
}
