<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctors extends Model
{
    public function clinics()
    {
        return $this->belongsToMany('App\Clinics');
    }
}
