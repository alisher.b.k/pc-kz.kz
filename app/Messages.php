<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Messages extends Model
{
    public function telegramUsers(){
        return $this->belongsToMany("App\TelegramUsers");
    }
}
