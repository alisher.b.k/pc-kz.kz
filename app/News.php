<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\NewsReactions;
class News extends Model
{
    public function newsReactions(){
       return $this->hasOne(NewsReactions::class);
    }
}
