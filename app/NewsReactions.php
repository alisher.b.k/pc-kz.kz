<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\News;
class NewsReactions extends Model
{
    public function news(){
       return $this->belongsTo(News::class);
    }
}
