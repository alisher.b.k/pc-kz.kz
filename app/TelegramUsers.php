<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Patient;
class TelegramUsers extends Model
{
    public function patient(){
        return $this->hasMany(Patient::class);
    }
    public function messages(){
        return $this->belongsToMany("App\Messages");
    }
}
