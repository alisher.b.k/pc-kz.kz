<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::middleware(['auth'])->prefix('admin')->namespace('Backend')->name('admin.')->group(function(){
    Route::get('/','DashboardController@index')->name('index');
    Route::get('/setting', 'SettingController@index')->name('setting.index');
    Route::post('/setting/store', 'SettingController@store')->name('setting.store');
    Route::post('setting/setwebhook', 'SettingController@setwebhook')->name('setting.webhook');
    Route::post('setting/getwebhookinfo', 'SettingController@getwebhookinfo')->name('setting.getwebhookinfo');
});
Route::post(Telegram::getAccessToken(), 'TelegramHandlerController@index')->name('telegram.index');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/updateStaff', 'StaffController@updateStaff')->name('updateStaff');
Route::get('/message', 'GroupController@index')->name('message.index');
Route::post('/message','GroupController@sendNotifications')->name('message.send');
Route::post('/message/type','GroupController@sendTypeOfMessage')->name('message.type');