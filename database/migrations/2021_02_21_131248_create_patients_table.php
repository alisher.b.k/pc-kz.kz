<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('telegram_users_id')->unique();
            $table->foreign('telegram_users_id')->references('id')->on('telegram_users');
            $table->string('insuredOrNot');
            $table->string('cardNumber')->nullable();
            $table->string('phoneNumber');
            $table->string('selectedClinic');
            $table->string('selectedDoctor');
            $table->string('reasonForTheRequest');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
