<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTelegramUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages_telegram_users', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('messages_id')->index();
            $table->foreign('messages_id')->references('id')->on('messages')->onDelete('cascade');

            $table->unsignedInteger('telegram_users_id')->index();
            $table->foreign('telegram_users_id')->references('id')->on('telegram_users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('messages_telegram_users');
    }
}
