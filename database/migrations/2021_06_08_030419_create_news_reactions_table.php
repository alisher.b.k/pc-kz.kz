<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsReactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_reactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reaction');
            $table->integer('telegramId');
            $table->unsignedInteger('news_id')->index();
            $table->foreign('news_id')->references('id')->on('news')->ondelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_reactions');
    }
}
