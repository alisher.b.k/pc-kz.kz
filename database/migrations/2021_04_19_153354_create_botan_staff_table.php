<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBotanStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('botan_staff', function (Blueprint $table) {
            $table->increments('id');
            $table->string('FIO')->nullable()->default(null);
            $table->string('position')->nullable()->default(null);
            $table->string('photo')->nullable()->default(null);
            $table->string('workPhone')->nullable()->default(null);
            $table->string('intPhone')->nullable()->default(null);
            $table->string('mobPhone')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->string('bday')->nullable()->default(null);
            $table->string('workEx')->nullable()->default(null);
            $table->string('ISN')->nullable()->default(null);
            $table->string('NAMELAT')->nullable()->default(null);
            $table->string('WORKPLACE')->nullable()->default(null);
            $table->string('MAINDEPT')->nullable()->default(null);
            $table->string('SUBDEPT')->nullable()->default(null);
            $table->string('VACATIONBEG')->nullable()->default(null);
            $table->string('VACATIONEND')->nullable()->default(null);
            $table->string('TRIPBEG')->nullable()->default(null);
            $table->string('TRIPEND')->nullable()->default(null);
            $table->string('LEAVEDATE')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('botan_staff');
    }
}
