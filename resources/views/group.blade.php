@extends('layouts.app')
@section('content')
<div class="container">
  <div class="card">
    <div class="card-header">
      <i class="fas fa-table"></i>
      Рассылка
    </div>
    <div class="card-body">
      <form action="{{ route('message.send') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="messageType">Тип</label>
          <select id="messageType" class="form-select" aria-label="Default select example" name="messageType">
            @foreach ($messages as $message)
                <option selected value="{{$message->name}}">{{$message->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="mt-2">
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="check" value="yes" id="buttonWithoutUnsubscribe">
            <label class="form-check-label" for="buttonWithoutUnsubscribe">
              Принудительное получение данной рассылки
            </label>
          </div>
          <div class="mt-2">
              <label>Количество подписанных пользователей :</label>
              <span id="count"></span>
          </div>
        <div class="form-group mt-2" >
          <label>Сообщение</label>
          <div class="input-group">
              <textarea class="form-control" name="message"></textarea>
          </div>
        </div>
        <div class="form-group mt-2 ">
          <label>Ссылка</label>
          <div class="input-group">
              <input class="form-control" name="link"></input>
          </div>
        </div>
        <div class="form-group mt-2">
          <label for="photo">Загрузить картинку</label><br>
          <input class="form-control-file" id="photo" name="photo" type="file">
        </div>
        <div class="input-group mt-2">
          <button type="submit" class="btn btn-primary">Отправить</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<<script type="text/javascript">
  $(document).ready(function(){ /* PREPARE THE SCRIPT */
    $("#messageType").change(function(){ /* WHEN YOU CHANGE AND SELECT FROM THE SELECT FIELD */
      var type = $(this).val(); /* GET THE VALUE OF THE SELECTED DATA */
      let _token   = $('meta[name="csrf-token"]').attr('content')
      $.ajax({ /* THEN THE AJAX CALL */
        type: "POST", /* TYPE OF METHOD TO USE TO PASS THE DATA */
        url: "{{ route('message.type')}}",
         /* PAGE WHERE WE WILL PASS THE DATA */
        data: {
          type:type,
          _token : _token
        }, /* THE DATA WE WILL BE PASSING */
        success:function(data){
          $('#count').text(data.success)
        },
      });

    });
  });
</script>